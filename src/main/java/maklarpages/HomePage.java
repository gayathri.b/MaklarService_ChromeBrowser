package maklarpages;

import static core.Action.click;
import static core.Action.sendKeys;
import static core.Action.verifyElementPresent;
import static core.Action.waitForElement;
import static org.junit.Assert.assertTrue;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import core.BasePage;
import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.qameta.allure.Link;
import io.qameta.allure.Step;

@Test
@Listeners({TestAllureListener.class})
public class HomePage extends BasePage {
	
	private BasePage basePage;
	public void setUp(WebDriver driver) throws Exception {
		 basePage = new BasePage();
		 driver = core.BasePage.initialize_driver(driver);
		 
		}
	
	@Test
	@Parameters("homepage")
	@Step("VerifyLinks")
	@JsonIgnoreProperties("Info")
	@Description("Verifying Text And Links On HomePAge")
	@Link("Text and Links on Homepage")
	@Attachment("Text and Links on HomePage")
public static void verifyTextAndLinkOnHomePage(WebDriver driver)throws Exception
	{
		Thread.sleep(1000);	//*[@id="header-bg"]/div/a[1]
		waitForElement(driver, By.cssSelector("#header-bg > a"));
		assertTrue("Presence of MaklarService Heading",verifyElementPresent(driver, By.cssSelector("#header-bg > a")));
		waitForElement(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle.svg"));
		assertTrue("Presence of menu icon",verifyElementPresent(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle.svg")));
		waitForElement(driver, By.linkText("Priser"));
		assertTrue("Presence of prices link",verifyElementPresent(driver, By.linkText("Priser")));
		click(driver, By.linkText("Priser"));
		Thread.sleep(2000);
		waitForElement(driver, By.linkText("Om oss"));
		assertTrue("Presence of About Us link",verifyElementPresent(driver, By.linkText("Om oss")));
		click(driver,By.linkText("Om oss"));
		Thread.sleep(1000);
		waitForElement(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle.svg"));
		click(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle.svg"));
		Thread.sleep(1000);
		waitForElement(driver, By.linkText("Integritetspolicy"));
		assertTrue("Presence of Integritetspolicy link",verifyElementPresent(driver, By.linkText("Integritetspolicy")));
		click(driver,By.linkText("Integritetspolicy"));
		Thread.sleep(2000);
		waitForElement(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle.svg"));
		click(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle.svg"));
		Thread.sleep(2000);
		waitForElement(driver, By.xpath("/html/body/div[3]/ul/li[2]/a"));
		assertTrue("Presence of Priser link",verifyElementPresent(driver, By.xpath("/html/body/div[3]/ul/li[2]/a")));
		click(driver,By.xpath("/html/body/div[3]/ul/li[2]/a"));
		Thread.sleep(2000);
		waitForElement(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle.svg"));
		click(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle.svg"));
		Thread.sleep(1000);
		waitForElement(driver, By.xpath("/html/body/div[3]/ul/li[3]/a"));
		assertTrue("Presence of Hem link",verifyElementPresent(driver, By.xpath("/html/body/div[3]/ul/li[3]/a")));
		click(driver,By.xpath("/html/body/div[3]/ul/li[3]/a"));
	}
		public static void verifySignInPage(WebDriver driver)throws Exception
		{
		Thread.sleep(1000);
		waitForElement(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle.svg"));
		click(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle.svg"));
		Thread.sleep(2000);
		waitForElement(driver, By.xpath("/html/body/div[3]/ul/li[4]/a"));
		assertTrue("Presence of Sign In link",verifyElementPresent(driver, By.xpath("/html/body/div[3]/ul/li[4]/a")));
		click(driver,By.xpath("/html/body/div[3]/ul/li[4]/a"));
	     
		// Sign In Process
		waitForElement(driver, By.xpath("//*[@id='user_email']"));
		sendKeys(driver, By.xpath("//*[@id='user_email']"),"gayathri.balagopal@cortexcraft.com");
		
		waitForElement(driver, By.xpath("//*[@id='user_password']"));
		sendKeys(driver, By.xpath("//*[@id='user_password']"),"Cortexcraft");
		
		waitForElement(driver, By.xpath("//*[@id='user_remember_me']"));
		click(driver, By.xpath("//*[@id='user_remember_me']"));
		
		waitForElement(driver, By.xpath("//*[@id='new_user']/input[3]"));
		click(driver, By.xpath("//*[@id='new_user']/input[3]"));
		
		// Broker Office information
		waitForElement(driver, By.xpath("//*[@id='switch-broker-office']"));
		Select brokerofc = new Select(driver.findElement(By.xpath("//*[@id='switch-broker-office']")));
		brokerofc.selectByIndex(1);
		
		//waitForElement(driver, By.xpath("/html/body/div[6]/div/div/table/tbody/tr[1]/td[9]/a"));
		//click(driver, By.xpath("/html/body/div[6]/div/div/table/tbody/tr[1]/td[9]/a"));
		
		//waitForElement(driver, By.xpath("//*[@id='switch-broker-office']"));
		//Select brokerofc1 = new Select(driver.findElement(By.xpath("//*[@id='switch-broker-office']")));
		//brokerofc1.selectByIndex(1);
		
		waitForElement(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle"));
		click(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle"));
		
	/*	Thread.sleep(1000);
		waitForElement(driver, By.xpath("/html/body/div[3]/ul/li[4]/a"));
		click(driver, By.xpath("/html/body/div[3]/ul/li[4]/a"));
		
		driver.navigate().refresh();
		
		waitForElement(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle"));
		click(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle"));
		*/
		Thread.sleep(2000);
		waitForElement(driver, By.xpath("/html/body/div[3]/ul/li[5]/a"));
		click(driver, By.xpath("/html/body/div[3]/ul/li[5]/a"));
		
		//waitForElement(driver, By.xpath("/html/body/div[5]/div/div/table/tbody/tr[1]/td[9]/a"));
	//	click(driver, By.xpath("/html/body/div[5]/div/div/table/tbody/tr[1]/td[9]/a"));
		
		Thread.sleep(2000);
		waitForElement(driver, By.xpath("//*[@id='switch-broker-office']"));
		Select brokerofcall = new Select(driver.findElement(By.xpath("//*[@id='switch-broker-office']")));
		brokerofcall.selectByIndex(1);
		Thread.sleep(1000);
		brokerofcall.selectByIndex(0);
		
		Thread.sleep(2000);
		
	//	waitForElement(driver, By.xpath("/html/body/div[6]/div/div/table/tbody/tr[1]/td[9]/a"));
	//	click(driver, By.xpath("/html/body/div[6]/div/div/table/tbody/tr[1]/td[9]/a"));
		
		waitForElement(driver, By.xpath("//*[@id='header-bg']/div/a[3]"));
		click(driver, By.xpath("//*[@id='header-bg']/div/a[3]"));
	}
		public static void validationVerifications(WebDriver driver)throws Exception
		{
		// validation Verifications
		Thread.sleep(2000);
		waitForElement(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle.svg"));
		click(driver, By.cssSelector("body > div.mobilemenu > div > div.toggle.svg"));
		
		Thread.sleep(2000);
		waitForElement(driver, By.xpath("/html/body/div[3]/ul/li[4]/a"));
		assertTrue("Presence of Sign In link",verifyElementPresent(driver, By.xpath("/html/body/div[3]/ul/li[4]/a")));
		click(driver,By.xpath("/html/body/div[3]/ul/li[4]/a"));
		
		Thread.sleep(2000);
		waitForElement(driver, By.xpath("//*[@id='user_email']"));
		sendKeys(driver, By.xpath("//*[@id='user_email']"),"gayathri.balagopal@cortexcraft.com");
		
		waitForElement(driver, By.xpath("//*[@id='user_password']"));
		sendKeys(driver, By.xpath("//*[@id='user_password']"),"Cortexcraf");
		
		waitForElement(driver, By.xpath("//*[@id='new_user']/input[3]"));
		click(driver, By.xpath("//*[@id='new_user']/input[3]"));
		
		
		
		
		
	}

}
